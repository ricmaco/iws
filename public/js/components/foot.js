define(function (require) {
    const Vue = require('vue');

    const s = require('i18n');

    Vue.component('foot', {
        props: [ "version" ],
        template: `
            <footer class="pt-3 text-center">
                <p class="mb-0">
                    {{s.foot1}}
                    <a href="https://github.com/napolux/paroleitaliane">{{s.foot2}}</a>
                </p>
                <p class="mb-0">
                    {{s.version}}{{version}}
                    -
                    {{s.code1}}
                    <a href="https://gitlab.com/ricmaco/iws">{{s.code2}}</a>
                    -
                    {{s.license}}
                </p>
                <p>
                    {{s.copyright}}
                    2020
                    <a href="mailto:r.macoratti@gmx.co.uk">Riccardo Macoratti</a>
                    (<a href="https://ricma.co">RicMa.co</a>)
                </p>
            </footer>
        `,
        data() {
            return { s: s }
        }
    });
});