define(function (require) {
    const Vue = require('vue');

    const s = require('i18n');

    Vue.component('intro', {
        template: `
            <div class="h3 text-center">
                <p>{{s.intro}}</p>
            </div>
        `,
        data() {
            return { s: s }
        }
    });
});