define(function (require) {
    const Vue = require('vue');

    const s = require('i18n');

    Vue.component('result', {
        props: [ 'data' ],
        template: `
            <div class="p-3 pt-4">
                <div
                    v-if="data"
                    class="card square mb-4"
                >
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">{{s.resultTitle}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="wrap">{{data}}</div>
                    </div>
                </div>
            </div>
        `,
        data() {
            return { s: s }
        }
    });
});