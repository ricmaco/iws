define(function(lang) {
    switch(lang) {
    case 'en':
    default:
        return {
            intro: 'Generate random lists of italian words, chosing from different sets.',
            fieldsSetLabel: 'Select a wordset',
            fieldsNumLabel1: 'Generating',
            fieldsNumLabel2: 'words',
            fieldsLengthAll: 'All',
            fieldsLengthLabelAll: 'Take all',
            fieldsLengthLabel: 'Longer than',
            fieldsLengthDesc: 'Select the desired length',
            fieldsSeparatorLabel: 'Provide a separator',
            fieldsSeparatorDesc: 'If empty a , will be used',
            fieldsSubmitLabel: 'Submit',
            resultTitle: 'Result',
            foot1: 'Some wordsets were borrowed from',
            foot2: 'this Github repository',
            version: 'v',
            code1: 'Read the',
            code2: 'code',
            license: 'MIT licensed',
            copyright: 'Copyright',
            'common-1000': '1000 most common words',
            'firstnames-9000': '9000 first names',
            'badwords-600': '600 bad words'
        }
    }
});