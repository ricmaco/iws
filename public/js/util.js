define(function(require) {
    const axios = require('axios');

    return {
        cachedFetchFile: async (id, url) => {
            // read from session or fall back to ajax
            if (sessionStorage && sessionStorage.getItem(id)) {
                return sessionStorage.getItem(id);
            }
            const res = await axios(url);
            sessionStorage.setItem(id, res.data);
            return res.data;
        },
        linesToArray: (lines) => (
            lines && lines.split("\n")
                .filter(x => x)
                .map(x => x.toLowerCase().trim())
        ),
        takeRandomN: (list, n) => (
            list && n && list
                .map((a) => ({sort: Math.random(), value: a}))
                .sort((a, b) => a.sort - b.sort)
                .map((a) => a.value)
                .slice(0, n)
        ),
        joinDefault: (list, sep, def) => (
            list && list.join(sep? sep : def)
        )
    };
});