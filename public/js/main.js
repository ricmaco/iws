require.config({
    paths: {
        "vue": "//unpkg.com/vue@2.6.11/dist/vue",
        "bootstrap-vue": "//unpkg.com/bootstrap-vue@2.11.0/dist/bootstrap-vue",
        "axios": "//unpkg.com/axios@0.19.2/dist/axios.min"
    },
    waitSeconds: 30
});

define(function (require) {
    const Vue = require('vue');
    const BootstrapVue = require('bootstrap-vue');
    Vue.use(BootstrapVue);
    
    const { cachedFetchFile, linesToArray, takeRandomN, joinDefault } = require('util');

    const s = require('i18n');
    const version = require('version');
    const res = require('res');

    require('components/navbar');
    require('components/intro');
    require('components/fields');
    require('components/result');
    require('components/foot');

    new Vue({
        el: '#app',
        template: `
            <div>
                <navbar />
                <div class="container-md">
                    <main class="pt-2">
                        <intro />
                        <article class="h5 pt-2">
                            <fields
                                :wordSets="wordSets"
                                :size="size"
                                :lengthLabels="lengthLabels"
                                :maxLength="maxLength"
                                :selectedSet.sync="selectedSet"
                                :separator.sync="separator"
                                :length.sync="length"
                                :wordsNum.sync="wordsNum"
                                :onChange="selectWords"
                            />
                            <result
                                :data="result"
                            />
                        </article>
                    </main>
                    <foot
                        :version="version"
                    />
                </div>
            </div>
        `,
        data() {
            return {
                s: s,
                version: version,
                wordSets: Object.keys(res).map(set => ({
                    text: s[set].toUpperCase(),
                    value: set
                })),
                selectedSet: Object.keys(res)[0],
                separator: ',',
                wordsNum: 100,
                maxLength: 0,
                length: 0,
                size: 0,
                result: ''
            }
        },
        methods: {
            async selectWords(event) {
                event && event.preventDefault && event.preventDefault();
                try {
                    // fetch via ajax
                    const data = await cachedFetchFile(this.selectedSet, res[this.selectedSet]);
                    
                    // convert to array and filter by length
                    const splitData = linesToArray(data).filter(w => w.length > this.length);
                    
                    // determine size and rescale number of considered words
                    this.size = splitData.length;
                    if (this.wordsNum > this.size) { this.wordsNum = this.size }

                    // calculate the maximum length to present to user
                    this.maxLength = Math.max( ...splitData.map(w => w.length) ) - 1;
                    
                    // take wordsNum random words and join them by separaton as result
                    this.result = joinDefault(takeRandomN(splitData, this.wordsNum), this.separator, ',');
                } catch (error) {
                    console.log(error);
                }
            },
            lengthLabels(len) {
                return len === 0
                    ? s.fieldsLengthAll
                    : len
            }
        },
        mounted() {
            this.selectWords();
        }
    });
});