define(function() {
    return {
        'common-1000': 'res/1000_parole_italiane_comuni.txt',
        'firstnames-9000': 'res/9000_nomi_propri.txt',
        'badwords-600': 'res/lista_badwords.txt'
    };
});