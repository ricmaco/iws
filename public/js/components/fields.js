define(function (require) {
    const Vue = require('vue');

    const s = require('i18n');

    Vue.component('fields', {
        props: [
            'wordSets',
            'size',
            'selectedSet',
            'separator',
            'lengthLabels',
            'length',
            'maxLength',
            'wordsNum',
            'onChange'
        ],
        template: `
            <div class="p-3">
                <b-form
                    id="gen-conf-form"
                    class="w-100 w-lg-50 mx-auto"
                    @submit="onChange"
                >
                    <b-form-group
                        id="gen-conf-form-group-set"
                        :label="s.fieldsSetLabel"
                        label-for="gen-conf-form-set"
                    >
                        <b-form-select
                            id="gen-conf-form-set"
                            form="gen-conf-form"
                            :value="selectedSet"
                            :options="wordSets"
                            @input="$emit('update:selectedSet', $event); onChange();"
                            required
                        >
                        </b-form-select>
                    </b-form-group>
                    <b-form-group
                        id="gen-conf-form-group-num"
                        :label="s.fieldsNumLabel1 + ' ' + wordsNum + ' ' + s.fieldsNumLabel2"
                        label-for="gen-conf-form-num"
                    >
                        <b-form-input
                            id="gen-conf-form-num"
                            form="gen-conf-form"
                            type="range"
                            min="1"
                            :max="size"
                            :value="wordsNum"
                            @input="$emit('update:wordsNum', $event);"
                            @change="onChange"
                            required
                        >
                        </b-form-input>
                    </b-form-group>
                    <div class="row">
                        <div class="col-sm-6">
                            <b-form-group
                                id="gen-conf-form-group-length"
                                class="text-center"
                                :label="length? s.fieldsLengthLabel : s.fieldsLengthLabelAll"
                                :description="s.fieldsLengthDesc"
                                label-for="gen-conf-form-length"
                            >
                                <b-form-spinbutton
                                    id="gen-conf-form-length"
                                    class="w-sm-50 mx-auto"
                                    form="gen-conf-form"
                                    :formatter-fn="lengthLabels"
                                    min="0"
                                    :max="maxLength"
                                    :value="length"
                                    @input="$emit('update:length', $event); onChange();"
                                >
                                </b-form-spinbutton>
                            </b-form-group>
                        </div>
                        <div class="col-sm-6">
                            <b-form-group
                                id="gen-conf-form-group-separator"
                                class="text-center"
                                :label="s.fieldsSeparatorLabel"
                                :description="s.fieldsSeparatorDesc"
                                label-for="gen-conf-form-separator"
                            >
                                <b-form-input
                                    id="gen-conf-form-separator"
                                    class="w-sm-50 mx-auto"
                                    form="gen-conf-form"
                                    type="text"
                                    :value="separator"
                                    @input="$emit('update:separator', $event); onChange();"
                                >
                                </b-form-input>
                            </b-form-group>
                        </div>
                    </div>
                </b-form>
            </div>
        `,
        data() {
            return {
                s: s
            }
        }
    });
});