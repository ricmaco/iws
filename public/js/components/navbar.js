define(function (require) {
    const Vue = require('vue');

    Vue.component('navbar', {
        template: `
            <b-navbar
                variant="muted"
                class="pt-1"
            >
                <b-navbar-brand
                    class="w-100 mr-0"
                    href="#"
                >
                    <b-img
                        center
                        src="img/logo-banner.png"
                        class="w-100 w-lg-50"
                        alt="iws logo"
                    />
                </b-navbar-brand>
            </b-navbar>
        `
    });
});